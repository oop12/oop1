/**
* @brief Encryption classının yaratıldığı, üye fonksiyon ve verilerinin oluşturulduğu kod parçası

*/

#pragma once
class Encryption
{
public:
	Encryption();
	int encrypt(int _value);
	int decrypt(int _value);
	int encdigit[4];
	int decdigit[4];

	~Encryption();
};

