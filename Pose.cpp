#include "Pose.h"

#include <iostream>
using namespace std;
Pose::Pose()
{
}

float Pose::getX()
{
	return x;
}

void Pose::setX(float _x)
{
	x = _x;
}

float Pose::getY()
{
	return y;
}

void Pose::setY(float _y)
{
	y = _y;
}

void Pose::setTh(float _th)
{
	th = _th;
}

float Pose::getTh()
{
	return th;
}
/*!\brief
Pose sınıfından turetilen nesnelerin esit olup olmadıklarını kontrol etmek icin operator== fonksiyonu yazildi.
*/
bool Pose::operator==(const Pose& other)
{
	if (x != other.x || y != other.y|| th!=other.th)
		return false;
	else
		return true;

}
/*!\brief
Pose sınıfından turetilen nesnelerin '+' isaret ile dogrudan toplanmasi saglandi.
*/
Pose Pose::operator+(const Pose &other)
{
	Pose tmp;
	tmp.x = x + other.x;
	tmp.y = y + other.y;
	tmp.th = th + other.th;
	return tmp;
}
/*!\brief
Pose sınıfından turetilen nesnelerin '-' isaret ile dogrudan cikarilmasi saglandi.
*/
Pose Pose::operator-(const Pose &other)
{
	Pose tmp;
	tmp.x = x - other.x;
	tmp.y = y - other.y;
	tmp.th = th + other.th;
	return tmp;
}

Pose & Pose::operator-=(const Pose &other)
{
	Pose tmp;
	x -= other.x;
	y -= other.y;
	th -= other.th;
	tmp.x = x;
	tmp.y = y;
	tmp.th = th;
	return tmp;

}

Pose & Pose::operator+=(const Pose &other)
{
	Pose tmp;
	x += other.x;
	y += other.y;
	th += other.th;
	tmp.x = x;
	tmp.y = y;
	tmp.th = th;
	return tmp;
}

bool Pose::operator<(const Pose &other)
{
	if (x < other.x || y < other.y||th<other.th)
		return false;
	else
		return true;
}

void Pose::getPose(float & _x, float & _y, float & _th)
{
	_x = x;
	_y = y;
	_th = th;
	cout << _x <<","<< _y <<","<< _th<<endl;
}

void Pose::setPose(float _x, float _y, float _th)
{
	x = _x;
	y = _y;
	th = _th;
}

Pose::~Pose()
{
}
