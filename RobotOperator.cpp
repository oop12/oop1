#include "RobotOperator.h"
#include "Encryption.h"


/// Robot oluşturulurken girilen 4 basamaklı sayıyı şifrelemede kullanılır.
/// int değer döndürür

int RobotOperator::encryptCode(int _value)
{
	Encryption tool;
	return tool.encrypt(_value);
}

/// Robota erişim sağlanmaya çalışıldığında şifrelenmiş erişim kodunu çözüp kullanıcının girdiği sayıyla kıyaslanabilir hale getirmek için kullanılır
///int değer döndürür
int RobotOperator::decryptCode(int _value)
{
	Encryption tool;
	return tool.decrypt(_value);
}
/// Constructor fonksiyon, oluşturulurken robota erişim için 4 basamaklı int bir sayı alır
RobotOperator::RobotOperator(int _accessCode)
{
		setAccessCode(_accessCode);
}
/// Private tanımlanan erişim kodunu çağırmada kullanılır
int RobotOperator::getAccessCode()
{
	return accessCode;
}
/// Robot oluşturulurken girilen 4 basamaklı int sayıyı şifrelemeye yarar
void RobotOperator::setAccessCode(int _accessCode)
{
	accessCode = encryptCode(_accessCode);
}
/// Robota erişim için kullanıcı bilgilerinin alınmasında kullanılır
void RobotOperator::setName(string _name)
{
	name = _name;
}

string RobotOperator::getName()
{
	return name;
}
/// Robota erişim için kullanıcı bilgilerinin alınmasında kullanılır
void RobotOperator::setSurname(string _surname)
{
	surname = _surname;
}

string RobotOperator::getSurname()
{
	return surname;
}

/// Robota erişim için girilen 4 basamaklı sayıyı erişim koduyla kıyaslar
/// Kıyaslama sonucunu erişim durumu isimli bir değişkene atar
bool RobotOperator::checkAccessCode(int _value)
{
	if (_value == decryptCode(getAccessCode()))
	{
		accessState = 1;
		return 1;
	}
	else
		accessState = 0;
		return 0;

}


/// Robota erişmeye çalışan kullanıcının bilgilerini ve erişim durumunu ekrana yazdırır.
void RobotOperator::print()
{
	cout << "Name Surname: " << getName() << " " << getSurname() << endl;
	if (accessState == 1)
	{
		cout << "Access Granted" << endl;
	}
	else
		cout << "Access Denied" << endl;
	
	system("pause");
}

RobotOperator::~RobotOperator()
{
}
