#include<string>
#include<iostream>
using namespace std;

/*
*@brief Robota erişim için kullanılan fonksiyon
*@param Isim ve Soyisim robota erişen kullanıcının bilgilerini tutar
*@param accessCode robotun oluşturulurken aldığı şifrelenmiş 4 basamaklı bir sayıyı tutar
*@param accessState kullanıcının girdiği sayıyı, şifresi çözülmüş accessCode ile kıyaslar
*/

class RobotOperator
{
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	int encryptCode(int _value);
	int decryptCode(int _value);
	
public:
	RobotOperator(int _accessCode);
	int getAccessCode();
	void setName(string _name);
	string getName();
	void setSurname(string _surname);
	string getSurname();
	void setAccessCode(int _accessCode);
	bool checkAccessCode(int _value);

	void print();

	~RobotOperator();
};

