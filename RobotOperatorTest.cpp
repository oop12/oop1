// Test Source For RobotOperator

// Every time you call print, print also calls "system("pause")" func.
#include "RobotOperator.h"
#include <iostream>

using namespace std;

int main()
{
	RobotOperator robot(3523);

	robot.setName("Funda");
	robot.setSurname("Gorgulu");
	robot.checkAccessCode(3523);
	robot.print();
	robot.setName("Ahmet");
	robot.setSurname("Cinar");
	robot.checkAccessCode(1353);
	robot.print();

}